/* ***************************************************************************
 *
 * Copyright 2019 Samsung Electronics All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific
 * language governing permissions and limitations under the License.
 *
 ****************************************************************************/

#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "st_dev.h"
#include "device_control.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_spi_flash.h"
#include "driver/uart.h"
#include "driver/gpio.h"

#include "iot_uart_cli.h"
#include "iot_cli_cmd.h"

#include "caps_switch.h"
#include "caps_voltageMeasurement.h"
#include "caps_powerMeter.h"
#include "caps_energyMeter.h"

#define TXD_PIN (GPIO_NUM_17)
#define RXD_PIN (GPIO_NUM_16)

#define USE_ST

/* Программа для работы со счетчиком Меркурий 206 через Оптопорт ("оптический" UART, не IrDA SIR) */

#define TIME_OUT_MSEC    3000
#define NELEMS(x)  (sizeof(x) / sizeof((x)[0]))
#define SERIAL_SIZE_RX 130

//#define COUNTER_SERIAL 46024039 // старый 206 RN
#define COUNTER_SERIAL 46345335 // новый с реле (206 PRNO)

//#define SKIP_ECHO_BYTES 1 // возможна ситуация, при которой в приемник попадает переданная посылка (эхо), соответственно определение этой переменной вырезает команду их начала ответа
// Примечание: эхо отсутствует когда черный оптосенскор повернут хвостом вверх


// onboarding_config_start is null-terminated string
extern const uint8_t onboarding_config_start[]    asm("_binary_onboarding_config_json_start");
extern const uint8_t onboarding_config_end[]    asm("_binary_onboarding_config_json_end");

// device_info_start is null-terminated string
extern const uint8_t device_info_start[]    asm("_binary_device_info_json_start");
extern const uint8_t device_info_end[]        asm("_binary_device_info_json_end");

static iot_status_t g_iot_status = IOT_STATUS_IDLE;
static iot_stat_lv_t g_iot_stat_lv;

IOT_CTX* ctx = NULL;

//#define SET_PIN_NUMBER_CONFRIM

static int noti_led_mode = LED_ANIMATION_MODE_IDLE;

static caps_switch_data_t *cap_switch_data;
static caps_voltageMeasurement_data_t *cap_voltage_data;
static caps_powerMeter_data_t *cap_power_data;
static caps_energyMeter_data_t *cap_energy_data_t1, *cap_energy_data_t2, *cap_energy_data_t3, *cap_energy_data_t4;

static int get_switch_state(void)
{
    const char* switch_value = cap_switch_data->get_switch_value(cap_switch_data);
    int switch_state = SWITCH_OFF;

    if (!switch_value) {
        return -1;
    }

    if (!strcmp(switch_value, caps_helper_switch.attr_switch.value_on)) {
        switch_state = SWITCH_ON;
    } else if (!strcmp(switch_value, caps_helper_switch.attr_switch.value_off)) {
        switch_state = SWITCH_OFF;
    }
    return switch_state;
}

static void cap_switch_cmd_cb(struct caps_switch_data *caps_data)
{
    int switch_state = get_switch_state();
    change_switch_state(switch_state);
}

static void capability_init()
{
    cap_switch_data = caps_switch_initialize(ctx, "main", NULL, NULL);
    if (cap_switch_data) {
        const char *switch_init_value = caps_helper_switch.attr_switch.value_on;

        cap_switch_data->cmd_on_usr_cb = cap_switch_cmd_cb;
        cap_switch_data->cmd_off_usr_cb = cap_switch_cmd_cb;

        cap_switch_data->set_switch_value(cap_switch_data, switch_init_value);
    }

    // voltage
    cap_voltage_data = caps_voltageMeasurement_initialize(ctx, "main", NULL, NULL);
    if (cap_voltage_data) {
        cap_voltage_data->set_voltage_unit(cap_voltage_data, caps_helper_voltageMeasurement.attr_voltage.unit_V);
        cap_voltage_data->set_voltage_value(cap_voltage_data, 230);
    }

    // power
    cap_power_data = caps_powerMeter_initialize(ctx, "main", NULL, NULL);
    if (cap_power_data) {
        cap_power_data->set_power_unit(cap_power_data, caps_helper_powerMeter.attr_power.unit_W);
        cap_power_data->set_power_value(cap_power_data, 1000);
    }

    // energy - T1
    cap_energy_data_t1 = caps_energyMeter_initialize(ctx, "T1", NULL, NULL);
    if (cap_energy_data_t1) {
        cap_energy_data_t1->set_energy_unit(cap_energy_data_t1, caps_helper_energyMeter.attr_energy.unit_kWh);
        cap_energy_data_t1->set_energy_value(cap_energy_data_t1, 1);
    }

    // energy - T2
    cap_energy_data_t2 = caps_energyMeter_initialize(ctx, "T2", NULL, NULL);
    if (cap_energy_data_t2) {
        cap_energy_data_t2->set_energy_unit(cap_energy_data_t2, caps_helper_energyMeter.attr_energy.unit_kWh);
        cap_energy_data_t2->set_energy_value(cap_energy_data_t2, 2);
    }

    // energy - T3
    cap_energy_data_t3 = caps_energyMeter_initialize(ctx, "T3", NULL, NULL);
    if (cap_energy_data_t3) {
        cap_energy_data_t3->set_energy_unit(cap_energy_data_t3, caps_helper_energyMeter.attr_energy.unit_kWh);
        cap_energy_data_t3->set_energy_value(cap_energy_data_t3, 3);
    }

    // energy - T4
    cap_energy_data_t4 = caps_energyMeter_initialize(ctx, "T4", NULL, NULL);
    if (cap_energy_data_t4) {
        cap_energy_data_t4->set_energy_unit(cap_energy_data_t4, caps_helper_energyMeter.attr_energy.unit_kWh);
        cap_energy_data_t4->set_energy_value(cap_energy_data_t4, 4);
    }
}

static void iot_status_cb(iot_status_t status,
                          iot_stat_lv_t stat_lv, void *usr_data)
{
    g_iot_status = status;
    g_iot_stat_lv = stat_lv;

    printf("status: %d, stat: %d\n", g_iot_status, g_iot_stat_lv);

    switch(status)
    {
        case IOT_STATUS_NEED_INTERACT:
            noti_led_mode = LED_ANIMATION_MODE_FAST;
            break;
        case IOT_STATUS_IDLE:
        case IOT_STATUS_CONNECTING:
            noti_led_mode = LED_ANIMATION_MODE_IDLE;
            change_switch_state(get_switch_state());
            break;
        default:
            break;
    }
}

#if defined(SET_PIN_NUMBER_CONFRIM)
void* pin_num_memcpy(void *dest, const void *src, unsigned int count)
{
    unsigned int i;
    for (i = 0; i < count; i++)
        *((char*)dest + i) = *((char*)src + i);
    return dest;
}
#endif

static void connection_start(void)
{
    iot_pin_t *pin_num = NULL;
    int err;

#if defined(SET_PIN_NUMBER_CONFRIM)
    pin_num = (iot_pin_t *) malloc(sizeof(iot_pin_t));
    if (!pin_num)
        printf("failed to malloc for iot_pin_t\n");

    // to decide the pin confirmation number(ex. "12345678"). It will use for easysetup.
    //    pin confirmation number must be 8 digit number.
    pin_num_memcpy(pin_num, "12345678", sizeof(iot_pin_t));
#endif

    // process on-boarding procedure. There is nothing more to do on the app side than call the API.
    err = st_conn_start(ctx, (st_status_cb)&iot_status_cb, IOT_STATUS_ALL, NULL, pin_num);
    if (err) {
        printf("fail to start connection. err:%d\n", err);
    }
    if (pin_num) {
        free(pin_num);
    }
}

static void connection_start_task(void *arg)
{
    connection_start();
    vTaskDelete(NULL);
}

static void iot_noti_cb(iot_noti_data_t *noti_data, void *noti_usr_data)
{
    printf("Notification message received\n");

    if (noti_data->type == IOT_NOTI_TYPE_DEV_DELETED) {
        printf("[device deleted]\n");
    } else if (noti_data->type == IOT_NOTI_TYPE_RATE_LIMIT) {
        printf("[rate limit] Remaining time:%d, sequence number:%d\n",
               noti_data->raw.rate_limit.remainingTime, noti_data->raw.rate_limit.sequenceNumber);
    }
}

void button_event(IOT_CAP_HANDLE *handle, int type, int count)
{
    if (type == BUTTON_SHORT_PRESS) {
        printf("Button short press, count: %d\n", count);
        switch(count) {
            case 1:
                if (g_iot_status == IOT_STATUS_NEED_INTERACT) {
                    st_conn_ownership_confirm(ctx, true);
                    noti_led_mode = LED_ANIMATION_MODE_IDLE;
                    change_switch_state(get_switch_state());
                } else {
                    if (get_switch_state() == SWITCH_ON) {
                        change_switch_state(SWITCH_OFF);
                        cap_switch_data->set_switch_value(cap_switch_data, caps_helper_switch.attr_switch.value_off);
                        cap_switch_data->attr_switch_send(cap_switch_data);
                    } else {
                        change_switch_state(SWITCH_ON);
                        cap_switch_data->set_switch_value(cap_switch_data, caps_helper_switch.attr_switch.value_on);
                        cap_switch_data->attr_switch_send(cap_switch_data);
                    }
                }
                break;
            case 5:
                /* clean-up provisioning & registered data with reboot option*/
                st_conn_cleanup(ctx, true);

                break;
            default:
                led_blink(get_switch_state(), 100, count);
                break;
        }
    } else if (type == BUTTON_LONG_PRESS) {
        printf("Button long press, iot_status: %d\n", g_iot_status);
        led_blink(get_switch_state(), 100, 3);
        st_conn_cleanup(ctx, false);
        xTaskCreate(connection_start_task, "connection_task", 2048, NULL, 10, NULL);
    }
}

static void app_main_task(void *arg)
{
    IOT_CAP_HANDLE *handle = (IOT_CAP_HANDLE *)arg;

    int button_event_type;
    int button_event_count;

    for (;;) {
        if (get_button_event(&button_event_type, &button_event_count)) {
            button_event(handle, button_event_type, button_event_count);
        }
        if (noti_led_mode != LED_ANIMATION_MODE_IDLE) {
            change_led_mode(noti_led_mode);
        }

        vTaskDelay(10 / portTICK_PERIOD_MS);
    }
}

/* EnergyMeter components start */

/*
    Функция для расчета CRC16 по схеме Modbus
*/
unsigned int crc16MODBUS(uint8_t *s, int from, int count)
{
  unsigned int crcTable[] = {
      0x0000, 0xC0C1, 0xC181, 0x0140, 0xC301, 0x03C0, 0x0280, 0xC241,
      0xC601, 0x06C0, 0x0780, 0xC741, 0x0500, 0xC5C1, 0xC481, 0x0440,
      0xCC01, 0x0CC0, 0x0D80, 0xCD41, 0x0F00, 0xCFC1, 0xCE81, 0x0E40,
      0x0A00, 0xCAC1, 0xCB81, 0x0B40, 0xC901, 0x09C0, 0x0880, 0xC841,
      0xD801, 0x18C0, 0x1980, 0xD941, 0x1B00, 0xDBC1, 0xDA81, 0x1A40,
      0x1E00, 0xDEC1, 0xDF81, 0x1F40, 0xDD01, 0x1DC0, 0x1C80, 0xDC41,
      0x1400, 0xD4C1, 0xD581, 0x1540, 0xD701, 0x17C0, 0x1680, 0xD641,
      0xD201, 0x12C0, 0x1380, 0xD341, 0x1100, 0xD1C1, 0xD081, 0x1040,
      0xF001, 0x30C0, 0x3180, 0xF141, 0x3300, 0xF3C1, 0xF281, 0x3240,
      0x3600, 0xF6C1, 0xF781, 0x3740, 0xF501, 0x35C0, 0x3480, 0xF441,
      0x3C00, 0xFCC1, 0xFD81, 0x3D40, 0xFF01, 0x3FC0, 0x3E80, 0xFE41,
      0xFA01, 0x3AC0, 0x3B80, 0xFB41, 0x3900, 0xF9C1, 0xF881, 0x3840,
      0x2800, 0xE8C1, 0xE981, 0x2940, 0xEB01, 0x2BC0, 0x2A80, 0xEA41,
      0xEE01, 0x2EC0, 0x2F80, 0xEF41, 0x2D00, 0xEDC1, 0xEC81, 0x2C40,
      0xE401, 0x24C0, 0x2580, 0xE541, 0x2700, 0xE7C1, 0xE681, 0x2640,
      0x2200, 0xE2C1, 0xE381, 0x2340, 0xE101, 0x21C0, 0x2080, 0xE041,
      0xA001, 0x60C0, 0x6180, 0xA141, 0x6300, 0xA3C1, 0xA281, 0x6240,
      0x6600, 0xA6C1, 0xA781, 0x6740, 0xA501, 0x65C0, 0x6480, 0xA441,
      0x6C00, 0xACC1, 0xAD81, 0x6D40, 0xAF01, 0x6FC0, 0x6E80, 0xAE41,
      0xAA01, 0x6AC0, 0x6B80, 0xAB41, 0x6900, 0xA9C1, 0xA881, 0x6840,
      0x7800, 0xB8C1, 0xB981, 0x7940, 0xBB01, 0x7BC0, 0x7A80, 0xBA41,
      0xBE01, 0x7EC0, 0x7F80, 0xBF41, 0x7D00, 0xBDC1, 0xBC81, 0x7C40,
      0xB401, 0x74C0, 0x7580, 0xB541, 0x7700, 0xB7C1, 0xB681, 0x7640,
      0x7200, 0xB2C1, 0xB381, 0x7340, 0xB101, 0x71C0, 0x7080, 0xB041,
      0x5000, 0x90C1, 0x9181, 0x5140, 0x9301, 0x53C0, 0x5280, 0x9241,
      0x9601, 0x56C0, 0x5780, 0x9741, 0x5500, 0x95C1, 0x9481, 0x5440,
      0x9C01, 0x5CC0, 0x5D80, 0x9D41, 0x5F00, 0x9FC1, 0x9E81, 0x5E40,
      0x5A00, 0x9AC1, 0x9B81, 0x5B40, 0x9901, 0x59C0, 0x5880, 0x9841,
      0x8801, 0x48C0, 0x4980, 0x8941, 0x4B00, 0x8BC1, 0x8A81, 0x4A40,
      0x4E00, 0x8EC1, 0x8F81, 0x4F40, 0x8D01, 0x4DC0, 0x4C80, 0x8C41,
      0x4400, 0x84C1, 0x8581, 0x4540, 0x8701, 0x47C0, 0x4680, 0x8641,
      0x8201, 0x42C0, 0x4380, 0x8341, 0x4100, 0x81C1, 0x8081, 0x4040};

  unsigned int crc = 0xFFFF;

  for (int i = from; i < count; i++)
  {
    crc = ((crc >> 8) ^ crcTable[(crc ^ s[i]) & 0xFF]);
  }
  return crc;
}


/*
    Функция для подготовки команды
*/
bool prepare_command(unsigned long int serial_number, unsigned char command_no, uint8_t *command, char *expected_answer_len)
{
  bool known_command = true;
  if (serial_number > 0xffffffff)
    return false;

  // серийный номер - 4 байта
  command[0] = (serial_number >> 24) & 0xff;
  command[1] = (serial_number >> 16) & 0xff;
  command[2] = (serial_number >> 8) & 0xff;
  command[3] = serial_number & 0xff;

  // команда - 1 байт
  switch(command_no) {
    case 0x27: // чтение содержимого тарифных аккумуляторов активной энергии
      *expected_answer_len = 23;
      break;
    case 0x28: // чтение идентификационных данных счетчика
      *expected_answer_len = 13;
      break;
    case 0x2f: // чтение серийного номера
      *expected_answer_len = 11;
      break;
    case 0x63: // чтение значений U, I, P
      *expected_answer_len = 14;
      break;
    case 0x66: // чтение даты изготовления
      *expected_answer_len = 10;
      break;
    default:
      *expected_answer_len = 11;
      known_command = false;
  }

  if (known_command)
  {
    command[4] = command_no;
  } 
  else
  {
    command[4] = 0x2f;
  }
  
  // вычисляем контрольную сумму
  unsigned int crc = crc16MODBUS(command, 0, 5);
  command[5] = crc & 0xFF;
  command[6] = (crc >> 8) & 0xFF;

  return known_command;
}

/*
  Функция для преобразования BCD в десятичный формат
*/
unsigned char bcd2dec(unsigned char bcd)
{
  if ((bcd >> 4) > 9)
    return 0;
  else if ((bcd & 0x0f) > 9)
    return 0;
  else
    return (bcd >> 4) * 10 + (bcd & 0x0f);
}

/* 
    Функция для парсинга ответа прибора
*/

bool parse_reply(int num_bytes, uint8_t *bytesReceived, unsigned long int serial_number, unsigned char command_no, unsigned char command_len, int max_index, char *str_out)
{
  sprintf(str_out, "%s", "error");

  // считаем контрольную сумму
  unsigned int crc = crc16MODBUS(bytesReceived, command_len, max_index - 2);
  unsigned int crc1 = crc & 0xFF;
  unsigned int crc2 = (crc >> 8) & 0xFF;
  
  // проверяем контрольную сумму
  if (crc1 == bytesReceived[max_index - 2] && crc2 == bytesReceived[max_index - 1]) {
    // считаем серийный номер
    char serial_number_bytes[4];
    serial_number_bytes[0] = (serial_number >> 24) & 0xff;
    serial_number_bytes[1] = (serial_number >> 16) & 0xff;
    serial_number_bytes[2] = (serial_number >> 8) & 0xff;
    serial_number_bytes[3] = serial_number & 0xff;

    // проверяем серийный номер в начале ответа
    if ( bytesReceived[command_len]   == serial_number_bytes[0] &&
         bytesReceived[command_len+1] == serial_number_bytes[1] &&
         bytesReceived[command_len+2] == serial_number_bytes[2] &&
         bytesReceived[command_len+3] == serial_number_bytes[3]) {

        switch(bytesReceived[command_len+4]) {
          case 0x27: // чтение содержимого тарифных аккумуляторов активной энергии
            sprintf(str_out, "%6.2f kW*h, %6.2f kW*h, %6.2f kW*h, %6.2f kW*h",
                             bcd2dec(bytesReceived[command_len+5])*10000 + bcd2dec(bytesReceived[command_len+6])*100 + bcd2dec(bytesReceived[command_len+7]) + bcd2dec(bytesReceived[command_len+8])*0.01,
                                          bcd2dec(bytesReceived[command_len+9])*10000 + bcd2dec(bytesReceived[command_len+10])*100 + bcd2dec(bytesReceived[command_len+11]) + bcd2dec(bytesReceived[command_len+12])*0.01,
                                                      bcd2dec(bytesReceived[command_len+13])*10000 + bcd2dec(bytesReceived[command_len+14])*100 + bcd2dec(bytesReceived[command_len+15]) + bcd2dec(bytesReceived[command_len+16])*0.01,
                                                                  bcd2dec(bytesReceived[command_len+17])*10000 + bcd2dec(bytesReceived[command_len+18])*100 + bcd2dec(bytesReceived[command_len+19]) + bcd2dec(bytesReceived[command_len+20])*0.01);

            cap_energy_data_t1->set_energy_value(cap_energy_data_t1, 
                                                                     bcd2dec(bytesReceived[command_len+5])*10000 + bcd2dec(bytesReceived[command_len+6])*100 + bcd2dec(bytesReceived[command_len+7]) + bcd2dec(bytesReceived[command_len+8])*0.01);
            cap_energy_data_t1->attr_energy_send(cap_energy_data_t1);

            cap_energy_data_t2->set_energy_value(cap_energy_data_t2, 
                                                                     bcd2dec(bytesReceived[command_len+9])*10000 + bcd2dec(bytesReceived[command_len+10])*100 + bcd2dec(bytesReceived[command_len+11]) + bcd2dec(bytesReceived[command_len+12])*0.01);
            cap_energy_data_t2->attr_energy_send(cap_energy_data_t2);

            cap_energy_data_t3->set_energy_value(cap_energy_data_t3, 
                                                                     bcd2dec(bytesReceived[command_len+13])*10000 + bcd2dec(bytesReceived[command_len+14])*100 + bcd2dec(bytesReceived[command_len+15]) + bcd2dec(bytesReceived[command_len+16])*0.01);
            cap_energy_data_t3->attr_energy_send(cap_energy_data_t3);

            cap_energy_data_t4->set_energy_value(cap_energy_data_t4, 
                                                                     bcd2dec(bytesReceived[command_len+17])*10000 + bcd2dec(bytesReceived[command_len+18])*100 + bcd2dec(bytesReceived[command_len+19]) + bcd2dec(bytesReceived[command_len+20])*0.01);
            cap_energy_data_t4->attr_energy_send(cap_energy_data_t4);
            break;
          
          case 0x28: // чтение идентификационных данных счетчика
            sprintf(str_out, "%d.%d (%02d.%02d.%04d)", bytesReceived[command_len+5], bytesReceived[command_len+6], /*bytesReceived[command_len+7],*/ bytesReceived[command_len+8], bytesReceived[command_len+9], bytesReceived[command_len+10] + 2000);
            break;
          
          case 0x2f: // чтение серийного номера
            sprintf(str_out, "%10d", (bytesReceived[command_len+5] << 24) | (bytesReceived[command_len+6] << 16) | (bytesReceived[command_len+7] << 8) | bytesReceived[command_len+8]);
            break;

          case 0x63: // чтение значений U, I, P
            sprintf(str_out, "%3.1f V, %2.2f A, %6d W", 
                              bcd2dec(bytesReceived[command_len+5])*10 + bcd2dec(bytesReceived[command_len+6])*0.1,
                                       bcd2dec(bytesReceived[command_len+7]) + bcd2dec(bytesReceived[command_len+8])*0.01,
                                                bcd2dec(bytesReceived[command_len+9])*10000 + bcd2dec(bytesReceived[command_len+10])*100 + bcd2dec(bytesReceived[command_len+11]));

            cap_voltage_data->set_voltage_value(cap_voltage_data, 
                                                                  bcd2dec(bytesReceived[command_len+5])*10 + bcd2dec(bytesReceived[command_len+6])*0.1);
            cap_voltage_data->attr_voltage_send(cap_voltage_data);

            cap_power_data->set_power_value(cap_power_data, 
                                                            bcd2dec(bytesReceived[command_len+9])*10000 + bcd2dec(bytesReceived[command_len+10])*100 + bcd2dec(bytesReceived[command_len+11]));
            cap_power_data->attr_power_send(cap_power_data);
            break;

          case 0x66: // чтение даты изготовления
            sprintf(str_out, "%02d.%02d.%04d", bcd2dec(bytesReceived[command_len+5]), bcd2dec(bytesReceived[command_len+6]), bcd2dec(bytesReceived[command_len+7]) + 2000);
            break;

          default:
            sprintf(str_out, "%s 0x%02X %s", "error: command ", bytesReceived[command_len+4], "is not supported!");
            return false;
        }
      
    } else {
      sprintf(str_out, "%s", "error: wrong serial number received!");
      return false;
    }
  } else {
    sprintf(str_out, "%s", "error: invalid checksum!");
    return false;
  }
  
  return true;
}

/* EnergyMeter components end */

void init_uart()
{
    const uart_config_t uart_config = {
        .baud_rate = 9600,
        .data_bits = UART_DATA_8_BITS,
        .parity = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
    };
    ESP_ERROR_CHECK(uart_param_config(UART_NUM_1, &uart_config));
    ESP_ERROR_CHECK(uart_set_pin(UART_NUM_1, TXD_PIN, RXD_PIN, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE));
    ESP_ERROR_CHECK(uart_driver_install(UART_NUM_1, SERIAL_SIZE_RX, SERIAL_SIZE_RX, 0, NULL, 0));
}

static void meter_task(void *arg)
{
    uint8_t my_command[7] = {};//{0x2, 0xc3, 0x2c, 0x77, 0x2f, 0xc6, 0x61};
    char expected_answer_len;

    for (;;) {
        int sc[] = {0x2f, /*0x2f, 0x2f, 0x28, */0x63, /*0x66,*/ 0x27};
        char command_no;

        for (int s=0; s<NELEMS(sc); s++)
        {
            command_no = sc[s];
            printf(" --- \n");
            printf("Sending command  0x%02x: ", sc[s]);

            prepare_command(COUNTER_SERIAL, command_no, my_command, &expected_answer_len);
            //printf("Will now send command: ");
            for (unsigned char h = 0; h < NELEMS(my_command); h++)
                printf(" 0x%02x", my_command[h]);
            printf("\n");

            if (command_no == 0x2f) {
                #if COUNTER_SERIAL == 46345335
                    char expected_answer[11] = {0x02, 0xc3, 0x2c, 0x77, 0x2f, 0x2, 0xc3, 0x2c, 0x77, 0xc0, 0x34};
                #else
                    char expected_answer[11] = {0x02, 0xbe, 0x45, 0x67, 0x2f, 0x02, 0xbe, 0x45, 0x67, 0x42, 0x9b};
                #endif

                printf("Expected answer bytes: ");
                for (unsigned char h = 0; h < NELEMS(expected_answer); h++)
                    printf(" 0x%02x", expected_answer[h]);
                printf("\n");
            }

            uart_write_bytes(UART_NUM_1, (const char*)my_command, NELEMS(my_command));

            // зеленый светодиод выключаем
            gpio_set_level(GPIO_ACT_LED, MAINLED_GPIO_OFF);

            uint8_t* bytesReceived = (uint8_t*) malloc(SERIAL_SIZE_RX+1);

            // принимаем ответ
            int byte_ind = uart_read_bytes(UART_NUM_1, bytesReceived, SERIAL_SIZE_RX,
                                           TIME_OUT_MSEC / portTICK_RATE_MS);

            if (byte_ind > 0) {
                bytesReceived[byte_ind] = 0; // End of received string

                char parsed_reply_str[SERIAL_SIZE_RX*2];
                #ifdef SKIP_ECHO_BYTES
                    int min_index = NELEMS(my_command);
                    int max_index = min(NELEMS(my_command) + expected_answer_len, NELEMS(bytesReceived));
                #else
                    int min_index = 0;
                    int max_index = expected_answer_len;
                #endif

                printf("       Received bytes:  ");
                for (unsigned int h = min_index; h < max_index; h++) 
                {
                    printf("0x%02x ", bytesReceived[h]);
                }
                printf("\n");

                bool parse_st = parse_reply(byte_ind, bytesReceived, COUNTER_SERIAL, command_no, min_index, max_index, parsed_reply_str);

                if (parse_st) {
                    printf("            Got value:  ");
                } else {
                    printf("            Got error:  ");
                }
                printf("%s\n", parsed_reply_str);

                // зеленый светодиод включаем
                gpio_set_level(GPIO_ACT_LED, MAINLED_GPIO_ON);
            }

            free(bytesReceived);
        }
        vTaskDelay(10000 / portTICK_PERIOD_MS);
    }
}

void app_main()
{
    init_uart();
    printf("EnergyMeter started, it is app_main.\n");

    /* Print chip information */
    esp_chip_info_t chip_info;
    esp_chip_info(&chip_info);
    printf("This is ESP32 chip with %d CPU cores, WiFi%s%s, ",
            chip_info.cores,
            (chip_info.features & CHIP_FEATURE_BT) ? "/BT" : "",
            (chip_info.features & CHIP_FEATURE_BLE) ? "/BLE" : "");

    printf("silicon revision %d, ", chip_info.revision);

    printf("%dMB %s flash.\n", spi_flash_get_chip_size() / (1024 * 1024),
            (chip_info.features & CHIP_FEATURE_EMB_FLASH) ? "embedded" : "external");

    /**
      SmartThings Device SDK(STDK) aims to make it easier to develop IoT devices by providing
      additional st_iot_core layer to the existing chip vendor SW Architecture.

      That is, you can simply develop a basic application
      by just calling the APIs provided by st_iot_core layer like below.

      // create a iot context
      1. st_conn_init();

      // create a handle to process capability
      2. st_cap_handle_init(); (called in function 'capability_init')

      // register a callback function to process capability command when it comes from the SmartThings Server.
      3. st_cap_cmd_set_cb(); (called in function 'capability_init')

      // process on-boarding procedure. There is nothing more to do on the app side than call the API.
      4. st_conn_start(); (called in function 'connection_start')
     */

  #ifdef USE_ST
    printf("SmartThings is starting.\n");
    unsigned char *onboarding_config = (unsigned char *) onboarding_config_start;
    unsigned int onboarding_config_len = onboarding_config_end - onboarding_config_start;
    unsigned char *device_info = (unsigned char *) device_info_start;
    unsigned int device_info_len = device_info_end - device_info_start;

    int iot_err;

    // create a iot context
    ctx = st_conn_init(onboarding_config, onboarding_config_len, device_info, device_info_len);
    if (ctx != NULL) {
        iot_err = st_conn_set_noti_cb(ctx, iot_noti_cb, NULL);
        if (iot_err)
            printf("fail to set notification callback function\n");
    } else {
        printf("fail to create the iot_context\n");
    }

    // create a handle to process capability and initialize capability info
    capability_init();

    iot_gpio_init();
    register_iot_cli_cmd();
    uart_cli_main();
    xTaskCreate(app_main_task, "app_main_task", 4096, NULL, 10, NULL);
  #endif

    xTaskCreate(meter_task, "meter_task", 4096, NULL, 11, NULL);

  #ifdef USE_ST
    // connect to server
    connection_start();
  #endif

}
