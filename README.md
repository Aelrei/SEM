# Программа Smart Energy Meter с использованием SmartThings SDK for Direct Connected Devices for C

## Введение

Эта программа обеспечивает непосредственное подключение (*direct-connected device*) устройства Smart Energy Meter к облаку Samsung SmartThings через Wi-Fi.

Устройство Smart Energy Meter позволяет считывать показания счетчика электрической энергии Меркурий 206 через интерфейс RS-485 или оптопорт и передавать через Wi-Fi данные в приложение Samsung SmartThings.

Используется плата Lilygo TTGO T1 на базе ESP32.

## Components & Capabilities

В компоненте *main* присутствуют следующие объекты:

* *healthCheck* для контроля "здоровья" нашего устройства;
* *Voltage Measurement* для отображения текущего значения напряжения сети;
* *Power Meter* для отображения текущего значения мощности нагрузки на выходе счетчика.

Далее следуют компоненты *T1*, *T2*, *T3* и *T4* с объектами *Energy Meter* для отображения значений потребления энергии по 4 тарифам.

## Используемые GPIO

Устройство использует следующие GPIO:

* линии RXD и TXD конвертера TTL RS-485 (или оптопорта) подключаются к соответствующим выводам `UART_NUM_1`;
* синий светодиод, отображающий состояние Wi-Fi, подключен к `GPIO18`;
* зеленый светодиод, отображающий прием данных от счетчика, подключен к `GPIO5`.

## Прошивка устройства

Для успешной компиляции содержимое этого Git-репозитория следует расположить внутри каталога `~/st-device-sdk-c-ref/apps/esp32/`, полученного и настроенного согласно [Getting Started](https://github.com/SmartThingsCommunity/st-device-sdk-c-ref/blob/master/doc/getting_started.md) в Debian 11 с помощью команд вида:

```
sudo apt-get install build-essential cmake python3-pip python-is-python3 git

cd ~
git clone https://github.com/SmartThingsCommunity/st-device-sdk-c-ref.git -b v1.7.0
cd ~/st-device-sdk-c-ref/apps/esp32/
git clone https://gitlab.com/Aelrei/SEM energy_meter
cd ~/st-device-sdk-c-ref
python setup.py esp32
```

Далее программа может быть загружена в плату с помощью команды `python build.py esp32 energy_meter flash`, а ее состояние в любой момент времени может быть оценено путем подключения к терминалу платы с помощью команды `python build.py esp32 energy_meter monitor`.
